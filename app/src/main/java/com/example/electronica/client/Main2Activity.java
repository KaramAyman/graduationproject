package com.example.electronica.client;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.File;

public class Main2Activity extends AppCompatActivity {

    ImageView imageView;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        imageView=(ImageView) findViewById(R.id.image2);
        File imgFile = new  File(getIntent().getStringExtra("image"));
        if(imgFile.exists())            {
            imageView.setImageURI(Uri.fromFile(imgFile));
        }
        //Bitmap bitmap = BitmapFactory.decodeFile(getIntent().getStringExtra("image"));
        //imageView.setImageBitmap(bitmap);
    }
}
